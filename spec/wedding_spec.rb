require 'wedding'

describe "Wedding#default_config" do
  it "returns default config options" do
    Wedding.default_config.should eql({
        :groom_name => "Nilesh",
        :groom_about => "Programmer, Geek, Traveller, Security Engineer",
        :groom_occupation => "Mobile, Cloud and Security Engineer at Oracle",
        :groom_email => "nilesh1883@gmail.com",
        :bride_name => "Sonali",
        :bride_about => "Programmer, Dreamer, Dancer, Foody",
        :bride_occupation => "Android Application Develoer at Diaspark",
        :bride_email => "sonali8890@gmail.com",
        :location => "22°41'28.5\"N 75°51'18.0\"E",
        :event_schedule => [
          "6:00 pm Barat starts from Mathura Mahal For Round to Nearby Temple",
          "7:00 pm Barat reaches back to Mathura Mahal",
          "12:00 pm Fere"
        ],
        :date => "29 January 2015"
      })
  end
end

describe "Wedding::Ceremony" do

  it "correctly initializes all the instance variables" do
  end

  it "launches the browser for map location" do
  end

  it "prints an awesome invitation message" do
  end

  it 'RSVPs for the event' do
  end

end
